
Dave's Website

Summary:
This webpage contains biographical information about David Winget, contact information, a guestbook, a quadratic equation calculator and a math trick. The purpose of this web page is to introduce myself and discuss my background, future plans, and interests.

Purpose:
This project was created to meet requirements of CSIS 44563, Developing Web Applications and Services, at Northwest Missouri State University. Specifically, it meets requirement A03 - Node.

Contributors:
This page was created by David Winget, S528390@nwmissouri.edu.

Using the website:
Open a command window in the A03 folder.
Run "node server" to start the server.  (Hit CTRL-C to stop.)
Point your browser to `http://localhost:8081`.
Routing -   Get "/" - display the default home page
            Get "/about" - display about page
            Get "/math" - display math page
            Get "/contact" - display contact page
            Get "/guestbook" - display all guestbook entries
            Get "/new-entry" - display a form for creating a new guestbook entry
            Post "/new-entry" - submit a new entry (add to an array of entries)

Acknowledgements:
- I started with a template from Initializr.
- I used the https://www.w3schools.com/tags/ website to get a description of each -HTML tag used and see an example of its use.
- I used the https://www.w3schools.com/css/ website to get a description of each CSS selector used and see an example of its use.
- I used the tutorial at http://tutsdaddy.com/how-to-send-email-from-nodejs-application-with-mailgun.html to send the email using Mailgun. 
- I took the .clearfix and *boxsizing code lines from the https://www.w3schools.com/css/css_float.asp website to help align my elements correctly.
- The valid number check in function calculateDigit (located in Math_trick.js) came from the slides in U03.
- I used the template from the M03 assignment for the Qunit test.
- I used the M04 assignment as a starting template for the Javascript files.
- I used the W07 assignment as a template for the guestbook and server.js file.
- The introductory video was an assignment I completed for course 62522.

Requirements:
An updated web browser.

Constructed with:
Visual Studio Code.

Running the tests:
To run the Quint test, go into the 'tests' subfolder and run Quint_tests.html.

License:
Copyright (c) 2017 David G. Winget. 

The GitHub, BitBucket, LinkedIn and Facebook logos are copyrighted by 
their respective websites.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.