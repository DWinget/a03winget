// Created by David Winget (usma94@gmail.com)
// Purpose: The purpose of this server file is to host my personal website.

// I used the W07 assignment as a template for this file.

var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser"); // simplifies access to request body
var app = express();  // make express app
var http = require('http').Server(app);  // inject app into the server

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Daves Website listening on http://127.0.0.1:8081/');
  console.log('');
  console.log('Navigation menu:');
  console.log('Get "/" - display default home page');
  console.log('Get "/about" - display about page');
  console.log('Get "/contact" - display contact page');
  console.log('Get "/math" - display math page');
  console.log('Get "/guestbook" - display guestbook page');
  console.log('Get "/new-entry" - display a form for creating a new guestbook entry');
  console.log('Post "/new-entry" - submit a new guestbook entry');
  console.log('');
});
app.use(express.static(__dirname + '/assets'));

// set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify view engine

// create an array to manage guestbook entries
var entries = [];
app.locals.entries = entries; 

// set up an http request logger to log every request automagically
app.use(logger("dev"));     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }));

// handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
    response.sendfile(path.join(__dirname + "/assets/index.html"));
});
app.get("/about", function (request, response) {
    response.sendfile(path.join(__dirname + "/assets/About_me.html"));
});
app.get("/contact", function (request, response) {
    response.render("contact");
});
app.get("/contactsuccess", function (request, response) {
    response.render("contactsuccess");
});
app.get("/contacterror", function (request, response) {
    response.render("contacterror");
});
app.get("/math", function (request, response) {
    response.sendfile(path.join(__dirname + "/assets/Math_page.html"));
});
app.get("/guestbook", function (request, response) {
    response.render("guestbook");
  });
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

// handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  console.log(request.body);
  if (!request.body.name || !request.body.body) {
   
    response.status(400).send("Entries must have a name and a body.");
    return;
  }
  entries.push({  // store it
    name: request.body.name,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");  
});

// handle an http POST request to send a contact message (send email)
app.post("/contact",function(req,res) {
    var api_key = 'key-fee6c413a79806f0e861703db00c2bcb';
    var domain = 'sandbox6fec0c728e7a44b6a34378e9df7e58ed.mailgun.org';
    var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
     
    var data = {
      from: 'Daves Website <postmaster@sandbox6fec0c728e7a44b6a34378e9df7e58ed.mailgun.org>',
      to: 'S528390@nwmissouri.edu',
      subject: 'Message from ' + req.body.firstname + ' ' + req.body.lastname + ' on Daves Website',
      html: "<b>Message: </b>" + req.body.contactmessage + "<hr><b>    From: </b>" + req.body.email
    };
    mailgun.messages().send(data, function (error, body) {
      console.log(body);
      if(!error){
        res.redirect('contactsuccess');
        console.log('Message successfully sent.');}
      else{
        res.redirect('contacterror');
        console.log('Error, message not sent. Please try again.');}
    });
});

// if we get a 404 status, render 404.ejs view
app.use(function (request, response) {
    response.status(404).render("404");
});